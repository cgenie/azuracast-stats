open Azuracast_stats
open CalendarLib

type working_mode =
  Statistics |
  ListenerCountGraph |
  ListenerStreamToggleCount

let date_of_string str =
  Scanf.sscanf str "%d-%d-%d" Date.make

let () =
  let today = Date.today () in
  (* http://calendar.forge.ocamlcore.org/doc/Printer.html *)
  let t = Printer.Date.sprint "%F" today in

  let api_key = ref "" in
  let bin_size = ref 15 in
  let debug = ref false in
  let ed = ref t in
  let mode = ref Statistics in
  let host = ref "" in
  let interval = ref 15 in
  let sd = ref "" in
  let station = ref 1 in

  let speclist = [
    ("--api-key", Arg.Set_string api_key, ": host");
    ("--bin-size", Arg.Set_int bin_size, ": bin size (for graph, in minutes)");
    ("--debug", Arg.Set debug, ": debug");
    ("--end", Arg.Set_string ed, ": end date");
    ("--interval", Arg.Set_int interval, ": interval");
    ("--host", Arg.Set_string host, ": host");
    ("--start", Arg.Set_string sd, ": start date");
    ("--station", Arg.Set_int station, ": station");
  ] in

  let parse_mode s =
    match s with
      | "statistics" -> Statistics
      | ""           -> Statistics
      | "listener-count" -> ListenerCountGraph
      | "listener-stream-toggle-count" -> ListenerStreamToggleCount
      | _            -> raise (Failure (Printf.sprintf "Unknown command %s (available commands: statistics [default], listener-count, listener-stream-toggle-count)" s))
  in

  let _ = Arg.parse speclist (fun s -> mode := parse_mode s) "USAGE" in

  (* Default interval for start/end date (in months) *)
  let default_period =
    match !mode with
      | Statistics -> 3
      | ListenerCountGraph -> 3
      | ListenerStreamToggleCount -> 3
    in
  let end_date = date_of_string !ed in
  let start_date = if !sd = "" then
    let p = Date.Period.month default_period in
    Date.rem end_date p
  else date_of_string !sd in

  let (p: params) = {
    host = !host;
    api_key = !api_key;
    start_date = start_date;
    end_date = end_date;
    station = !station;
  } in

  let listeners = Lwt_main.run (fetch_listener_statistics p) in

  match !mode with
    | Statistics -> show_statistics listeners start_date end_date
    | ListenerCountGraph -> show_listener_count_graph listeners !bin_size
    | ListenerStreamToggleCount -> show_listener_stream_toggle_count listeners !bin_size
