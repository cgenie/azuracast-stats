# Fetch `azuracast-stats`

This program fetches stats from Azuracast API and prints them.

# Compiling

Assuming you have Ocaml's [Opam](https://opam.ocaml.org/):

```
opam switch create azuracast-stats 4.07.1
opam install dune utop ssl tls lwt lwt_ppx lwt_ssl uri yojson ipaddr calendar
opam install cohttp cohttp-lwt-unix
dune build azuracaststats.exe
./_build/default/bin/azuracaststats.exe
```
