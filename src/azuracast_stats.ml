open Cohttp
open Cohttp_lwt_unix
open CalendarLib


type params = {
  host : string;
  api_key: string;
  start_date: Date.t;
  end_date: Date.t;
  station: int;
}


type listener = {
  ip: Ipaddr.V4.t;
  user_agent: string;
  is_mobile: bool;
  connected_on: int;    (* When he was connected (unixtime) *)
  connected_time: int;  (* How long listener was connected, in seconds *)
  (*
  "location": {
    "status": "success",
    "lat": 50.0838,
    "lon": 21.4314,
    "timezone": "Europe/Warsaw",
    "region": "Subcarpathian",
    "country": "Poland",
    "city": "Kedzierz",
    "message": "This product includes GeoLite2 data created by MaxMind, available from <a href=\"http://www.maxmind.com\">http://www.maxmind.com</a>."
  }
  *)
  connection_end: int;  (* At what time did the user disconnected, calculated based on connected_on/time *)
}

type range = {
  min: int;
  max: int;
}

let in_r (x : int) (r : range) = r.min <= x && x <= r.max
let intersects (r1 : range) (r2 : range) = r2.min <= r1.max && r1.min <= r2.max

let parse_listener l =
  let open Yojson.Basic.Util in

  let connected_on_int = l |> member "connected_on" |> to_int in
  let connected_time_int = l |> member "connected_time" |> to_int in
  let connection_end_int = connected_on_int + connected_time_int in
  (* let to_gmtime i = i |> Float.of_int |> Unix.gmtime in *)

  {
    ip=(l |> member "ip" |> to_string |> Ipaddr.V4.of_string_exn);
    user_agent=(l |> member "user_agent" |> to_string);
    is_mobile=(l |> member "is_mobile" |> to_bool);
    connected_on=connected_on_int;
    connected_time=connected_time_int;
    connection_end=connection_end_int;
  }

let parse_listeners b =
  let js = Yojson.Basic.from_string b in

  Yojson.Basic.Util.convert_each parse_listener js

let fetch_listener_statistics (p: params) =
  let path = Printf.sprintf "/api/station/%d/listeners" p.station in

  let uri'' = Uri.of_string p.host in
  let uri' = Uri.with_path uri'' path in
  let uri = Uri.with_query uri' [("start", [Printer.Date.to_string p.start_date]); ("end", [Printer.Date.to_string p.end_date])] in
  (* let uri = p.host |> Uri.of_string |> Uri.with_path path |> Uri.with_query [("start", [p.start_date]); ("end", [p.end_date])] in *)

  let headers = Header.of_list [
    ("Authorization", "Bearer " ^ p.api_key)
  ] in

  let%lwt (_, body) = Client.get uri ~headers:headers in
  let%lwt b = Cohttp_lwt.Body.to_string body in

  (* let%lwt _ = Lwt_io.printf "uri: %s\n" (Uri.to_string uri) in *)

  let parsed = parse_listeners b in

  Lwt.return parsed


(* Shows list of connected hosts in the given time period *)
let show_statistics listeners start_date end_date =
  let s = ref 0 in
  List.iter (fun l ->
    Printf.printf "ip: %s; connected_time: %d\n" (Ipaddr.V4.to_string l.ip) l.connected_time;
    s := !s + l.connected_time
  ) listeners;

  Printf.printf "Period: %s -- %s\n" (Printer.Date.to_string start_date) (Printer.Date.to_string end_date);
  Printf.printf "Total time: %d (%f listener hours)\n" !s ((float_of_int !s) /. 3600.0)


let hash_add h key value =
  let cs = match Hashtbl.find_opt h key with
           | Some contents -> contents
           | None          -> []
  in Hashtbl.add h key (value :: cs)

let hash_add_key_range h (key_r : range) value =
  let arr = Array.init (key_r.max - key_r.min + 1) (fun i -> key_r.min + i) in
  (* Array.fold_left (fun h_ key -> hash_add h_ key value; h_) h arr *)
  Array.iter (fun key ->
    hash_add h key value
  ) arr

(* Group listeners into bins *)
(* TODO access_func should return a range and listener should be added to
   however many bins in that range he belongs to. *)
let get_listener_bins listeners bin_size access_func =
  (* Assign all listeners into their bins. *)
  (* Use division by bin_size to determine into which bin a listener belongs. *)
  let bins = Hashtbl.create (List.length listeners) in
  let bin_id unixtime = unixtime / bin_size in
  let rev_bin_id bin_id = bin_id * bin_size in

  List.iter (fun l ->
    let r = access_func l in
    let bin_min = bin_id r.min in
    let bin_max = bin_id r.max in
    hash_add_key_range bins {min=bin_min; max=bin_max} l
  ) listeners;

  let min_bin_id = ref (bin_id (Unix.time () |> int_of_float) + 1) in
  let max_bin_id = ref 0 in
  let keys = Hashtbl.to_seq_keys bins in

  Seq.iter (fun key ->
    if !min_bin_id > key then
      min_bin_id := key;
    if !max_bin_id < key then
      max_bin_id := key
  ) keys;

  let bin_array = Array.init (!max_bin_id - !min_bin_id + 1) (fun num ->
    let idx = !min_bin_id + num in
    let bin =
      match Hashtbl.find_opt bins idx with
      | Some bin -> bin
      | None     -> []
    in
    (idx, rev_bin_id idx, bin)
  ) in

  bin_array

(* Group listeners into bins. However only min/max bins are added and not the
   whole range (as in get_listener_bins) *)
let get_listener_toggle_bins listeners bin_size access_func =
  (* Assign all listeners into their bins. *)
  (* Use division by bin_size to determine into which bin a listener belongs. *)
  let bins_min = Hashtbl.create (List.length listeners) in
  let bins_max = Hashtbl.create (List.length listeners) in
  let bin_id unixtime = unixtime / bin_size in
  let rev_bin_id bin_id = bin_id * bin_size in

  List.iter (fun l ->
    let r = access_func l in
    let bin_min = bin_id r.min in
    let bin_max = bin_id r.max in
    hash_add bins_min bin_min l;
    hash_add bins_max bin_max l
  ) listeners;

  let min_bin_id = ref (bin_id (Unix.time () |> int_of_float) + 1) in
  let max_bin_id = ref 0 in

  Seq.iter (fun key ->
    if !min_bin_id > key then
      min_bin_id := key;
  ) (Hashtbl.to_seq_keys bins_min);
  Seq.iter (fun key ->
    if !max_bin_id < key then
      max_bin_id := key;
  ) (Hashtbl.to_seq_keys bins_max);

  let bin_array = Array.init (!max_bin_id - !min_bin_id + 1) (fun num ->
    let idx = !min_bin_id + num in
    let min_bins =
      match Hashtbl.find_opt bins_min idx with
      | Some bin -> bin
      | None     -> []
    in
    let max_bins =
      match Hashtbl.find_opt bins_max idx with
      | Some bin -> bin
      | None     -> []
    in
    (idx, rev_bin_id idx, min_bins, max_bins)
  ) in

  bin_array

(* Generates gnuplot data *)
let show_listener_count_graph listeners bin_size =
  let bin_array = get_listener_bins listeners bin_size (fun l ->
    {min=l.connected_on; max=l.connection_end}
  ) in

  (* CSV header *)
  Printf.printf "date,number of listeners\n";
  (* TODO This isn't correct -- only connected_on is used, however I should also
     report listeners in all bins in the duration of (connected_on,
     connection_end) *)

  Array.iter (fun (_idx, unixtime, bin) ->
    let cal_time = unixtime |> Float.of_int |> Unix.gmtime |> Calendar.from_unixtm in
    let listener_count = List.length bin in

    if listener_count > 0 then
      Printf.printf "%s,%d\n" (Printer.Calendar.to_string cal_time) (List.length bin)
  ) bin_array

  (*
  List.iter (fun l ->
    let connected_on_lt = l.connected_on |> Float.of_int |> Unix.gmtime in
    let connected_on_c = connected_on_lt |> Calendar.from_unixtm in
    Printf.printf "ip: %s; connected_on: %s\n" (Ipaddr.V4.to_string l.ip) (Printer.Calendar.to_string connected_on_c)
  ) listeners
   *)

let show_listener_stream_toggle_count listeners bin_size =
  let bin_array = get_listener_toggle_bins listeners bin_size (fun l ->
    {min=l.connected_on; max=l.connection_end}
  ) in

  Printf.printf "date,connected listeners,disconnected listeners\n";

  Array.iter (fun (_idx, unixtime, min_bin, max_bin) ->
    let cal_time = unixtime |> Float.of_int |> Unix.gmtime |> Calendar.from_unixtm in
    let listeners_connected = List.length min_bin in
    let listeners_disconnected = List.length max_bin in

    if (listeners_connected > 0) || (listeners_disconnected > 0) then
      Printf.printf "%s,%d,%d\n" (Printer.Calendar.to_string cal_time) (List.length min_bin) (List.length max_bin)
  ) bin_array
